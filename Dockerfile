FROM fox-base:latest

RUN mkdir /opt/app/logs && chmod +x /opt/app/logs
ADD . /opt/app

CMD ["python", "manage.py"]
