"""Entrypoint to the bot.

1. Place an initial buy around the lowest possible bid.
2. When this executes, place a sell order for a percentage of profit, then
3. Place a buy offer below the initial buy.
4. if the buy executes first, place another sell.
5. If the sell executes first, find the lowest sell order and place a
    buy beneath that order that maintains the straddle space. If there
    are no sell orders, close all positions and enter the market again.
"""
import logging
import gdax

from constants import GDAX_SUBSCRIPTIONS
from mongo import get_client
from gdax_stream import StreamWebsocketClient
from util import get_credentials, parse_secret, setup_logging

LOGGING = setup_logging(logging, 'manage')
LOGGER = LOGGING.getLogger(__name__)


def exchanage_factory(exchange, key, secret, passphrase=None):
    """Return the requested exchange interface.

    exchange (str): The name of the exchange.
    key (str): The api key.
    secret (str): The api secret.
    """
    return {
        'gdax': gdax.AuthenticatedClient(key, secret, passphrase)
    }.get(exchange)


def connect_gdax(channels, database=None, retries=0, **auth_data):
    """Connect and subscribe to the GDAX websocket feed."""
    if database is not None:
        stream = StreamWebsocketClient(database=database)
        LOGGER.info('Websocket initialised with mongodb...')
    else:
        stream = StreamWebsocketClient()
        LOGGER.info('Websocket initialised...')

    stream.subscribe(channels, **auth_data)

    while not stream.connection.connected:
        if retries <= 5:
            LOGGER.warning('Attempting websocket reconnection...')
            return connect_gdax(
                channels,
                database=database,
                retries=retries,
                **auth_data
            )
        else:
            LOGGER.info(
                'Failed to connect to GDAX after %s retries',
                str(retries)
            )


def main():
    """Entrypoint."""
    # Get credentials.
    resp = get_credentials('GDAX')
    authentication = parse_secret(resp, 'GDAX')

    # Include a mongodb connection
    session = get_client('mongo', 27017)
    gdaxdb = session.gdax

    connect_gdax(GDAX_SUBSCRIPTIONS, database=gdaxdb, **authentication)
    LOGGER.info('Closing GDAX connection')

if __name__ == '__main__':
    main()
