"""API wrapper for poloniex. Adapted from https://pastebin.com/fbkheaRb"""

import hashlib
import hmac
import json
import time
from datetime import datetime, timedelta
from urllib.parse import urlencode

import requests

from util import datetime_to_timestamp

PRIVATE_ENDPOINT = 'https://poloniex.com/tradingApi'
PUBLIC_ENDPOINT = "https://poloniex.com/public"


def filter_tickers(tickers, required=None):
    """Filter dictionaries based on key.

    Args:
        tickers (list): A list of Poloniex ticker dicts.
        required (list): A list if ticker pairs. ['USDT_LTC', 'BTC_LTC']
    Returns:
        A generator of dicts.
    """
    def required_pair(ticker):
        """Return True if the dict keys are in required."""
        return ticker['pair'] in required

    return filter(required_pair, tickers) if required is not None else tickers


def has_resulting_trades(buy_order):
    """Return True if the buy is immediately filled."""
    return True if any(buy_order['resultingTrades']) else False


def is_funded(order):
    """Check that the response from a buy order has sufficient funds."""
    errors = order.get('error')
    if errors is None:
        return False
    return not errors.lower().startswith('not enough')


def is_successful(cancellation):
    """Return True if the cancellation is successful."""
    print('order to cancel: {}'.format(cancellation))
    return cancellation['success'] == 1


def is_valid_amount(amount, fee=0.25):
    """Return True if the order amount is valid.

    Args:
        rate (float): The price of the coin.
        amount (float): The amount of coin to buy, expressed as a decimal.
    Returns:
        TYPE (bool): The total order value must be more than 0.0001
        sell: 15%
        buy: 25%
    """
    order_value = amount - (amount * fee)
    return order_value >= 0.0001


def trade_history_payload(currency_pair, start=None, end=None):
    """Return the payload to be sent to the trade history endpoint.

    Args:
        start (datetime): The start datetime
        end (datetime): The end datetime
        currency_pair (str): The currency pair e.g. 'BTC_XCP' or `ALL`
            for all markets.

    Returns:
        Payload (dict): Dict containing `start`, `end`, `currencyPair`
            and `command` keys.
    """
    payload = {
        'currencyPair': currency_pair,
        'command': 'returnTradeHistory'
    }
    if start is None:
        delta = datetime.utcnow() - timedelta(days=1)
        payload['start'] = datetime_to_timestamp(delta)
    else:
        payload['start'] = datetime_to_timestamp(start)

    if end is None:
        payload['end'] = datetime_to_timestamp(datetime.utcnow())
    else:
        payload['end'] = datetime_to_timestamp(end)
    return payload


def transform_response(response):
    """Yield a dict of floats and the ticker name.

    Args:
        response (dict): A dictionary of pair: values
        E.g. "USDT_LTC": {
                "id": 123,
                "last": "44.45000003",
                ...
    Yields:
        A sequence of values from the response updated with the pair key.
    """
    for pair, values in response.items():
        new_values = {}
        for name, value in values.items():
            new_values[name] = float(value)
        new_values['pair'] = pair
        yield new_values


class Poloniex(object):
    """Interface into both public and private Poloniex API methods."""

    def __init__(self, api_key, api_secret):
        self.api_key = api_key
        self.api_secret = api_secret

    def api_query(self, public=True, **payload):
        """
        Main API query method.

        Args:
            headers (dict): The headers in the form {
                'Key': 'thekey',
                'Sign': 'f291471940174n1209n4nn213084327cb...',
                'content-type': 'application/x-www-form-urlencoded'
            }
            public (bool): Determines the endpoint to query.
            payload (dict): Keyword params to be dumped to json. Usually
                features {'command': '<api method>', }
        """
        if public:
            return requests.get(PUBLIC_ENDPOINT, params=payload).json()

        payload['nonce'] = int(time.time() * 1000)
        try:
            response = requests.post(
                PRIVATE_ENDPOINT,
                data=urlencode(payload, 'utf-8'),
                headers=self.make_headers(**payload)
            )
        except requests.exceptions.ConnectTimeout:
            self.api_query(**payload)
        except requests.exceptions.RequestException as exc:
            raise requests.exceptions.RequestException(exc)
        return response.json()

    def close_open_positions(self, currency_pair, open_positions):
        """Close all open orders on the exchange."""
        # Maybe add logging to the db and annotations to influx for
        # cancellations?
        # logger.debug('Open positions: %s', open_positions)
        if any(open_positions):
            for position in open_positions:
                # logger.info('Order cancelled: %s', position.order_number)
                self.cancel(currency_pair, position.order_number)

    def get_balance(self, coin_ticker):
        """Return the balance for a specific coin."""
        balances = self.balances()
        return balances.get(coin_ticker)

    def balances(self, public=False):
        """Returns all of your balances.

        Args:
            public (bool): Whether the method is exposed via the public
            api.
        Returns:
            {'BTC':'0.59098578','LTC':'3.31117268', ... }
        """
        return self.api_query(
            public=public,
            **{'command': 'returnBalances'}
        )

    def buy(self, currency_pair, rate, amount, public=False):
        """Places a buy order in a given market.

        Required POST parameters are 'currency_pair', 'rate', and
        'amount'. If successful, the method will return the order number.

        Args:
            currencyPair  The curreny pair
            rate          price the order is buying at
            amount        Amount of coins to buy
        Returns:
            dict: Contains the orderNumber and resultingTrades.
        """
        total_amount = abs(rate * amount)
        if is_valid_amount(total_amount, fee=0.25):
            print('is valid amount')
            order = self.api_query(
                public=public,
                **{
                    'command': 'buy',
                    'currencyPair': currency_pair,
                    'rate': abs(rate),
                    'amount': abs(amount)
                }
            )
            if not is_funded(order):
                raise InsufficientFunds(json.dumps(order))
            return order

        print('Raising exception')
        raise InvalidOrder(
            'The value ({}) of the order is too low.'.format(amount))

    def cancel(self, currency_pair, order_number, public=False):
        """Cancels an order you have placed in a given market.

        Required POST parameters are 'currencyPair' and 'orderNumber'.

        Args:
            currencyPair  The curreny pair
            orderNumber   The order number to cancel
        Returns:
            {
                'error': 'Invalid order number,
                or you are not the person who placed the order.',
                'success': 0
            }
        """
        cancellation = self.api_query(
            public=public,
            **{
                'command': 'cancelOrder',
                'currencyPair': currency_pair,
                'orderNumber': order_number
            }
        )
        if is_successful(cancellation):
            return cancellation
        else:
            message = 'Failed to cancel order {}'.format(order_number)
            raise OrderNotCancelled(message)

    @staticmethod
    def invalid_cancellations(cancellations):
        """Returns any failed cancellations"""
        return filter(lambda c: c.get('success') != 1, cancellations)

    def cancel_orders(self, currency_pair, orders):
        """Handle cancelling a sequence of orders."""
        # cancel = partial(self.cancel, currency_pair=currency_pair, **orders)
        for order in orders:
            cancellation = self.cancel(currency_pair, order['orderNumber'])
            print('The cancellation: {}'.format(cancellation))
            yield cancellation

    def make_headers(self, **payload):
        """Return the headers for the API call."""
        return {
            'Sign': hmac.new(
                bytes(self.api_secret, 'utf-8'),
                bytes(urlencode(payload), 'utf-8'),
                digestmod=hashlib.sha512
            ).hexdigest(),
            'Key': str(self.api_key),
            'content-type': 'application/x-www-form-urlencoded'
        }

    def market_trade_history(self, currency_pair):
        """Query the API for trading history for a currency pair."""
        return self.api_query(
            'returnMarketTradeHistory',
            **{'currencyPair': currency_pair}
        )

    def open_orders(self, currency_pair, public=False):
        """Returns your open orders for a given market.

        Args:
            currencyPair  The currency pair e.g. 'BTC_XCP'
        Returns:
            dict of {market: order}, pairs if currency_pair is 'all'
            list of open orders as dicts if the currency_pair is specified
                e.g 'BTC_LTC'

            orderNumber   The order number
            type          sell or buy
            rate          Price the order is selling or buying at
            Amount        Quantity of order
            total         Total value of order (price * quantity)
            E.g: 'BTC_LTC': [
                {
                    'amount': '0.12000000',
                    'date': '2017-08-02 14:19:28',
                    'margin': 0,
                    'orderNumber': '96012642299',
                    'rate': '0.00157148',
                    'startingAmount': '0.12000000',
                    'total': '0.00018857',
                    'type': 'buy'
                }
            ],
        """
        return self.api_query(
            public=public,
            **{'currencyPair': currency_pair, 'command': 'returnOpenOrders'}
        )

    def order_book(self, currency_pair):
        """Query the API for the order book using a currency pair"""
        return self.api_query(
            **{'command': 'returnOrderBook', 'currencyPair': currency_pair}
        )

    @staticmethod
    def valid_trade(trade):
        """Return True if the trade response has no errors."""
        return any(
            [
                trade.get('error') is None,
                any(
                    [
                        trade.get('type') == 'buy',
                        trade.get('type') == 'sell'
                    ]
                )
            ]
        )

    def order_trades(self, order_number):
        """Return the trades from `order_number`."""
        return self.api_query(
            **{'command': 'returnOrderTrades', 'orderNumber': order_number}
        )

    def sell(self, currency_pair, rate, amount, public=False):
        """Places a sell order in a given market.

        Required POST parameters are "currencyPair", "rate", and
        "amount". If successful, the method will return the order
        number.

        Args:
            currencyPair  The curreny pair
            rate          price the order is selling at
            amount        Amount of coins to sell
        Returns:
            orderNumber (dict): {'orderNumber': '96606976412',
            'resultingTrades': []}
        """
        sell_coin_ticker, _ = currency_pair.split('_')
        total = abs(rate * amount)
        print('{} sell amount: {}'.format(sell_coin_ticker, total))

        if is_valid_amount(total, fee=0.15):
            return self.api_query(
                public=public,
                **{
                    'command': 'sell',
                    'currencyPair': currency_pair,
                    'rate': abs(rate),
                    'amount': abs(amount)
                }
            )
        raise InvalidOrder(
            'The value ({}) of the order is too low.'.format(total))

    def ticker(self, public=True, **payload):
        """Query the API for ticker info."""
        return self.api_query(public=public, **payload)

    def trade_history(self, currency_pair, start=None, end=None, public=False):
        """TODO: **This is useless at the moment as it's not possible to
        pass in a time range and get a response other than an empty
        dict.**

        Returns your trade history for a given market, specified by the
        'currency_pair' POST parameter

        Returns: (dict)
            date          Date in the form: '2014-02-19 03:44:59'
            rate          Price the order is selling or buying at
            amount        Quantity of order
            total         Total value of order (price * quantity)
            type          sell or buy
        """
        return self.api_query(
            public=public,
            **trade_history_payload(currency_pair, start=start, end=end)
        )

    def volume_24h(self, **payload):
        """Query the API for 24H Volume info."""
        return self.api_query("return24Volume", **payload)

    def withdraw(self, currency, amount, address, public=False):
        """Immediately places a withdrawal for a given currency.

        No email confirmation. In order to use this method, the
        withdrawal privilege must be enabled for your API key.
        Required POST parameters are "currency", "amount", and
        "address".

        Sample output: {"response":"Withdrew 2398 NXT."}

        Args:
            currency      The currency to withdraw
            amount        The amount of this coin to withdraw
            address       The withdrawal address
        Returns:
            response      Text containing message about the withdrawal
        """
        return self.api_query(
            public=public,
            **{
                'command': 'withdraw',
                'currency': currency,
                'amount': amount,
                'address': address
            }
        )


class PoloniexException(Exception):
    """Base from which to derive further exceptions."""

    def __init__(self, message, exception=None):
        self.message = message
        self.exception = exception
        super().__init__(message)


class InvalidOrder(PoloniexException):
    """The order is not of a sufficient amount."""
    pass


class OrderNotCancelled(PoloniexException):
    """Raised when an existing order fails to cancel."""
    pass


class InsufficientFunds(PoloniexException):
    """Insufficient funds to create a sell order."""
