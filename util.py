"""Utilities module."""
import json
import os
from datetime import datetime
from functools import lru_cache
from urllib.parse import urljoin

from credstash import getSecret


@lru_cache(maxsize=5, typed=False)
def get_credentials(secret, profile_name=None):
    """Parse and return a string literal from credstash."""
    return getSecret(secret, profile_name=profile_name, region='eu-west-1')


def parse_secret(secret, name=None):
    """Return a specific secret from the parsed string."""
    data = json.loads(secret)
    if name is None:
        return data
    return data[name]


def datetime_to_timestamp(date_time):
    """Convert a datetime object to unixtimestamp (milliseconds)

    Args:
        date_time (datetime): Datetime object.
    Returns:
        unix timestamp (int): timestamp in milliseconds.
    """
    epoch = datetime.utcfromtimestamp(0)
    return int((date_time - epoch).total_seconds() * 1000.0)


def get_logfile_name(filepath):
    """Dynamically return the path to the logfile."""
    filepath = filepath.split('/')
    fname = '.'.join([filepath[-1], 'log'])
    filepath = filepath[:-1]
    filepath.append('logs')
    filepath = '/'.join(filepath)
    return urljoin(filepath + '/', fname)


def get_stop_limit(initial_price, current_price):
    """Exit based on hostile market conditions.

    If the price has fallen more than 30%, or starting balance is
    depleted by 40% or more, exit and inform.
    """
    # current price as pct of initial buy
    difference = current_price - initial_price
    return (difference / initial_price) * 100


def setup_logging(logging, file_name):
    """Return logging configuration."""
    logging.basicConfig(
        filename=get_logfile_name(os.path.abspath(file_name)),
        level=logging.DEBUG,
        format='%(asctime)s::%(levelname)s::%(name)s::%(message)s'
    )

    logging.getLogger('botocore').setLevel(logging.CRITICAL)
    logging.getLogger('boto3').setLevel(logging.CRITICAL)
    logging.getLogger('urllib3').setLevel(logging.CRITICAL)

    return logging
