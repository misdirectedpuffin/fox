"""Interface to the GDAX websocket API."""
import base64
import hashlib
import hmac
import json
import logging
import time
from threading import Thread

import gdax
from websocket import WebSocketConnectionClosedException, create_connection

LOGGER = logging.getLogger(__name__)


class StreamWebsocketClient(gdax.WebsocketClient):
    """Websocket Client to connect to GDAX stream."""

    def __init__(self, url='wss://ws-feed.gdax.com', database=None):
        self.url = url
        self.database = database
        self.message_count = 0
        self.stop = False
        self._auth = None
        self._connection = None
        super().__init__()

    @property
    def connection(self):
        """Create or return existing websocket."""
        if self._connection is None:
            LOGGER.info('Creating new Websocket connection...')
            self._connection = create_connection(self.url)
        return self._connection

    @staticmethod
    def create_message(timestamp):
        """Create the message for authentication."""
        message = timestamp + 'GET' + '/users/self/verify'
        message = message.encode('ascii')
        return message

    @staticmethod
    def create_signature(message, secret):
        """Return the hashed signature

        Args:
            message (str): The ascii encoded message string.
            secret (str): The base64 encoded secret key.
        Return:
            signature_b64 (str): The encoded signature.
        """
        hmac_key = base64.b64decode(secret)
        signature = hmac.new(hmac_key, message, hashlib.sha256)
        signature_b64 = base64.b64encode(signature.digest())
        return signature_b64

    def auth(self, timestamp, key, secret, passphrase):
        """Return a dict of encoded data to be used for authentication.

        Args:
            key (str): the api key.
            secret (str): the base64 api secret key
            passphrase (str): the passphrase provided during generation
                of the api keys.
        """
        message = self.create_message(timestamp)
        signature_b64 = self.create_signature(message, secret)
        return {
            'signature': signature_b64.decode('ascii'),
            'key': key,
            'passphrase': passphrase,
            'timestamp': timestamp
        }

    @staticmethod
    def create_channel(channel, product_ids):
        """Return a channel item to subscribe to."""
        channel = {'name': channel, 'product_ids': product_ids}
        if channel['name'] == 'heartbeat':
            return dict(channel, **{"on": True})
        return channel

    def _get_collection(self, message):
        """Return the collection for the message type received."""
        order_type = message.get('type')
        return self.database[order_type]

    def _insert_message(self, message):
        """Insert a json message from the api into mongodb collection."""
        if self.database is not None:
            collection = self._get_collection(message)
            collection.insert_one(message)
        return

    def _make_subscriptions(self, timestamp, channels, **auth_data):
        """Return a list of subscriptions to send to the websocket.

        Args:
            channels (list): list of (channel, product) tuples.
            auth_data (dict): Dict of key, secret and passphrase to be
                encoded and passed to the websocket for authentication.
        Returns:
            subscriptions (list): A list of dicts in the form:
                [
                    {"name": "heartbeat", "product_ids": self.products},
                    {"name": "ticker", "product_ids": self.products}
                ]
        """
        subscriptions = [self.create_channel(c[0], c[1]) for c in channels]
        sub_params = {
            "type": "subscribe",
            "channels": subscriptions
        }

        key = auth_data.get('key')
        secret = auth_data.get('secret')
        passphrase = auth_data.get('passphrase')

        if all([secret, key, passphrase]):
            sub_params.update(**self.auth(timestamp, key, secret, passphrase))
        return sub_params

    def subscribe(self, channels, **auth_data):
        """Send subscription data to the websocket api."""
        self.on_open()
        subscriptions = self._make_subscriptions(
            str(time.time()),
            channels,
            **auth_data
        )
        self.connection.send(json.dumps(subscriptions))
        LOGGER.info('Subscribing to %s', json.dumps(channels))
        try:
            self.thread = Thread(target=self.listen())
            self.thread.start()
        except Exception as exc:
            LOGGER.error('Thread failed to start: %s', str(exc))
        else:
            LOGGER.info('Thread started...')

    def listen(self):
        """Receive the data from the websocket."""
        while self.connection.connected:
            time.sleep(0.1)
            try:
                if int(time.time() % 30) == 0:
                    # Set a 30 second ping to keep connection alive
                    self.connection.ping("keepalive")
                    LOGGER.info('Pinging websocket to \'keepalive\'')

                data = self.connection.recv()
                message = json.loads(data)
            except WebSocketConnectionClosedException as exc:
                self.on_error(exc)
            except Exception as exc:
                self.on_error(exc)
            else:
                self.on_message(message)

    def start(self, subscriptions):
        LOGGER.warning('this method is deprecated, please use subscribe()).')
        self.subscribe(subscriptions)

    def close(self):
        if self.stop:
            self.on_close()
            self.stop = True
            self.connection.close()
            LOGGER.info('Websocket connection closed.')

    def on_open(self):
        pass

    def on_close(self):
        pass

    def on_error(self, exc):
        LOGGER.warning('Failed to receive data from websocket: %s', str(exc))
        self.connection.close()

    def on_message(self, message):
        self._insert_message(message)
        LOGGER.debug(message)


def get_websocket_client():
    """Return a connection to the streaming client."""
    return StreamWebsocketClient(
        url="wss://ws-feed.gdax.com",
    )


# def enter_market(session, exchange, price, num_coins,
# currency_pair='BTC_LTC'):
#     """Make a buy order on the exchange."""
#     LOGGER.info('----- Checking DB for open Positions -----')
#     # Check for open positions.

#     LOGGER.info('----- Closing all open Positions -----')
#     Close open positions.

#     try:
#         # Make the buy order.
#     except InsufficientFunds:
#         LOGGER.warning('Insufficient funds!')
#         return False
#     else:
#         return buy_order
