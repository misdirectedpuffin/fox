"""Unit tests related to the GDAX websocket client."""
import pytest

from gdax_stream import StreamWebsocketClient


def test_create_message():
    """It returns the correct ascii encoded string."""
    client = StreamWebsocketClient()
    assert client.create_message('12345') == b'12345GET/users/self/verify'


def test_create_signature():
    """It returns the hashed signature."""
    client = StreamWebsocketClient()
    assert client.create_signature(
        '12345GET/users/self/verify'.encode('ascii'),
        'MTIzNDVHRVQvdXNlcnMvc2VsZi92ZXJpZnk='
    ) == b'TnycGhWrsbaJJbuNK2MNte77mkNJNev/FtPKZQQM2JY='


def test_auth():
    """It returns the correct authentication data."""
    client = StreamWebsocketClient()
    assert client.auth(
        '12345678',
        'thetestkey',
        'MTIzNDVHRVQvdXNlcnMvc2VsZi92ZXJpZnk=',
        'thepassphrase'
    ) == {
        'key': 'thetestkey',
        'passphrase': 'thepassphrase',
        'signature': 'xwgZTxrp7A2i0GyP8GAdAKAMI6n29OXYhGY/Ep8d+Is=',
        'timestamp': '12345678'
    }


def test_create_channel():
    """It returns the correct channel item."""
    client = StreamWebsocketClient()
    actual = client.create_channel('foo', ['LTC-USD', 'LTC-BTC'])
    expected = {'name': 'foo', 'product_ids': ['LTC-USD', 'LTC-BTC']}
    assert actual == expected

@pytest.mark.parametrize('timestamp, channels, auth, expected', [
    (
        '1514158625.3147347',
        [('foo', ['LTC-USD'])],
        {},
        {
            "type": "subscribe",
            "channels": [
                {
                    'name': 'foo',
                    'product_ids': ['LTC-USD']
                }
            ]
        }
    ),
    (
        '1514158625.3147347',
        [('foo', ['LTC-USD'])],
        {
            "key": "thetestkey",
            "passphrase": "thepassphrase",
            "secret": "MTIzNDVHRVQvdXNlcnMvc2VsZi92ZXJpZnk="
        },
        {
            "type": "subscribe",
            "channels": [
                {
                    'name': 'foo',
                    'product_ids': ['LTC-USD']
                }
            ],
            'key': 'thetestkey',
            'passphrase': 'thepassphrase',
            'signature': 'OMHtarx/6UyVPuHIXhZH54AiRwNnEf1Faai68E5UE4o=',
            'timestamp': '1514158625.3147347'
        }
    )
])
def test_make_subscriptions(timestamp, channels, auth, expected):
    """It returns the correct subscription."""
    client = StreamWebsocketClient()
    assert client._make_subscriptions(
        timestamp,
        channels,
        **auth
    ) == expected
