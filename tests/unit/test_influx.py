"""Tests related to influxdb"""
import json
from datetime import datetime
from textwrap import dedent
from unittest import mock

import influxdb
import pytest
from freezegun import freeze_time

from influx import (GDAXAnnotation, gdax_datapoint_factory,
                    has_database, list_databases, InfluxPoloniex)


@pytest.mark.parametrize('item, expected', [
    (
        # influx item
        {
            'id': 123,
            'last': '44.45000003',
            'lowestAsk': '44.63999795',
            'highestBid': '44.45000003',
            'percentChange': '-0.00214379',
            'baseVolume': '8363760.30505012',
            'quoteVolume': '182616.92200237',
            'isFrozen': '0',
            'high24hr': '47.98899999',
            'low24hr': '43.66310794',
            'pair': 'USDT_LTC'
        },
        {
            'measurement': 'USDT_LTC',
            'tags': {
                'ticker': 'USDT_LTC'
            },
            'time': '2017-07-27T00:00:00Z',
            'fields': {
                'id': 123,
                'last': '44.45000003',
                'lowestAsk': '44.63999795',
                'highestBid': '44.45000003',
                'percentChange': '-0.00214379',
                'baseVolume': '8363760.30505012',
                'quoteVolume': '182616.92200237',
                'isFrozen': '0',
                'high24hr': '47.98899999',
                'low24hr': '43.66310794'
            }
        }
    ),
    # ewma item.
    (
        {
            'last': 42.11000001,
            'measurement': 'ewma',
            'field': 'last',
            'pair': 'USDT_LTC',
            'time': '2017-07-27T00:00:00Z'
        },
        {
            'measurement': 'ewma',
            'tags': {
                'ticker': 'USDT_LTC',
                'ewma': 'last'
            },
            'time': '2017-07-27T00:00:00Z',
            'fields': {'last': 42.11000001}
        }
    )
])
@freeze_time('2017-07-27')
def test_make_influx_datapoint(item, expected):
    """Test that a single datapoint is made correctly."""
    influx = InfluxPoloniex()
    assert influx.make_poloniex_influx_datapoint(**item) == expected


@pytest.mark.xfail
def test_create_annotations():
    """It returns the correct Poloninex annotation."""
    assert False


def test_gdax_datapoint_factory(mock_mongo_ticker_document):
    """It returns the correct influx datapoint."""
    assert gdax_datapoint_factory(**mock_mongo_ticker_document)() == {
        'tags': {
            'product_id': 'LTC-EUR',
            'type': 'ticker',
            'side': 'sell'
        },
        'fields': {
            "sequence": 395474229,
            "price": "241.03000000",
            "open_24h": "244.38000000",
            "volume_24h": "26926.88332587",
            "low_24h": "241.03000000",
            "high_24h": "248.00000000",
            "volume_30d": "2885272.65252122",
            "best_bid": "241.02",
            "best_ask": "241.21",
            "trade_id": 1682115,
            "last_size": "0.05544123"
        },
        'time': "2017-12-26T17:21:51.244000Z",
        'measurement': 'ticker'
    }


def test_make_ewma_records(mock_influxdb_response):
    """Test that the ewma is the same as a Series of mock_tickers."""
    influx = InfluxPoloniex()
    assert list(influx.make_ewma_records(
        mock_influxdb_response,
        'USDT_LTC',
        'last',
        span=3
    )) == [
        {
            'last': 42.11000001,
            'measurement': 'ewma',
            'field': 'last',
            'pair': 'USDT_LTC',
            'time': '2017-07-26T00:00:47Z'
        },
        {
            'last': 42.11000001,
            'measurement': 'ewma',
            'field': 'last',
            'pair': 'USDT_LTC',
            'time': '2017-07-26T00:01:47Z'
        },
        {
            'last': 42.204914267142854,
            'measurement': 'ewma',
            'field': 'last',
            'pair': 'USDT_LTC',
            'time': '2017-07-26T00:02:47Z'
        },
        {
            'last': 42.25562666333334,
            'measurement': 'ewma',
            'field': 'last',
            'pair': 'USDT_LTC',
            'time': '2017-07-26T00:03:47Z'
        },
        {
            'last': 42.401856044838716,
            'measurement': 'ewma',
            'field': 'last',
            'pair': 'USDT_LTC',
            'time': '2017-07-26T00:04:47Z'
        },
        {
            'last': 42.48523075222223,
            'measurement': 'ewma',
            'field': 'last',
            'pair': 'USDT_LTC',
            'time': '2017-07-26T00:05:47Z'
        }
    ]


def test_make_querystring():
    """Test that the querystring is made correctly."""
    influx = InfluxPoloniex()
    options = {
        'measurement': 'ewma',
        'field': 'last',
        'ticker': 'USDT_LTC',
        'start': datetime(2017, 1, 1, 1, 1, 30),
        'end': datetime(2017, 1, 2, 1, 1, 5)
    }
    assert influx.make_querystring(**options) == dedent('''
        SELECT "last" FROM "ewma" WHERE \
        "ticker" = 'USDT_LTC' AND \
        time > '2017-01-01 01:01:30' AND \
        time <= '2017-01-02 01:01:05' \
        GROUP BY "ticker"
    ''')


@pytest.mark.parametrize(
    'fields, expected', [
        # Without date arg specified.
        (
            {
                'title': 'the title',
                'text': 'the text',
                'tags': 'the,tags',
            },
            {
                'measurement': 'events',
                'time': '2017-07-27 00:00:00',
                'fields': {
                    'title': 'the title',
                    'text': 'the text',
                    'tags': 'the,tags'
                }
            }
        ),
        # With date arg specified.
        (
            {
                'title': 'the title',
                'text': 'the text',
                'tags': 'the,tags',
                'date_time': datetime(2017, 7, 25)
            },
            {
                'measurement': 'events',
                'time': '2017-07-25 00:00:00',
                'fields': {
                    'title': 'the title',
                    'text': 'the text',
                    'tags': 'the,tags'
                }
            }
        ),
        # Tags not specified
        (
            {
                'title': 'the title',
                'text': 'the text',
                'date_time': datetime(2017, 7, 25)
            },
            {
                'measurement': 'events',
                'time': '2017-07-25 00:00:00',
                'fields': {
                    'title': 'the title',
                    'text': 'the text',
                }
            }
        )
    ]
)
@freeze_time('2017-07-27')
def test_annotate(fields, expected):
    """Test that an annotation is created correctly."""
    influx = InfluxPoloniex()
    assert influx.annotate(**fields) == expected


def test_make_tag_format_string():
    """It returns the correct string of tags."""
    annotation = GDAXAnnotation(**{})
    mock_tags = ['foo', 'bar']
    assert annotation._make_tag_format_string(
        tags=mock_tags) == '{bar},{foo},{price},{product_id},{side},{type}'


@freeze_time('2017-07-27')
def test_check_timestamp_none():
    """It returns utc timestamp when 'time' key is none."""
    annotation = GDAXAnnotation(**{})
    assert annotation.check_timestamp() == '2017-07-27T00:00:00.000000Z'


def test_check_timestamp_not_none():
    """It returns the timestamp when it is not none."""
    annotation = GDAXAnnotation(**{'time': '2017-07-27T00:00:00.000000Z'})
    assert annotation.check_timestamp() == '2017-07-27T00:00:00.000000Z'


def test_ticker(mock_mongo_ticker_document):
    """It returns the correct influxdb data."""
    annotation = GDAXAnnotation(**mock_mongo_ticker_document)
    assert annotation.ticker() == {
        'tags': {
            'product_id': 'LTC-EUR',
            'side': 'sell',
            'type': 'ticker'
        },
        'fields': {
            "sequence": 395474229,
            "price": "241.03000000",
            "open_24h": "244.38000000",
            "volume_24h": "26926.88332587",
            "low_24h": "241.03000000",
            "high_24h": "248.00000000",
            "volume_30d": "2885272.65252122",
            "best_bid": "241.02",
            "best_ask": "241.21",
            "trade_id": 1682115,
            "last_size": "0.05544123"
        },
        'time': '2017-12-26T17:21:51.244000Z',
        'measurement': 'ticker'
    }


@pytest.mark.parametrize('order, tags, expected', [
    (
        {
            "type": "open",
            "side": "sell",
            "price": "300.00000000",
            "order_id": "617b5d68-d262-413a-852a-4420c1208e4b",
            "remaining_size": "8.75900000",
            "product_id": "LTC-EUR",
            "sequence": 393435435,
            "user_id": "59ac1146e2c4ee02d62e13f5",
            "profile_id": "c077a092-38b2-4a98-8359-08c70d31fbeb",
            "time": "2017-12-25T00:45:37.141000Z"
        },
        'foo,bar',
        {
            'measurement': 'open',
            'time': '2017-12-25T00:45:37.141000Z',
            'fields': {
                'title': 'open order',
                'text': json.dumps(
                    {
                        "type": "open",
                        "side": "sell",
                        "price": "300.00000000",
                        "order_id": "617b5d68-d262-413a-852a-4420c1208e4b",
                        "remaining_size": "8.75900000",
                        "product_id": "LTC-EUR",
                        "sequence": 393435435,
                        "user_id": "59ac1146e2c4ee02d62e13f5",
                        "profile_id": "c077a092-38b2-4a98-8359-08c70d31fbeb",
                        "time": "2017-12-25T00:45:37.141000Z"
                    }
                ),
                'tags': 'foo,bar'
            }
        }
    )
])
def test_make_annotation(order, tags, expected):
    """It creates the correct annotation."""
    annotation = GDAXAnnotation(**order)
    assert annotation._make_annotation(tags) == expected


def test_open_order(mock_gdax_open_order):
    """It returns an annotation for an open order."""
    annotation = GDAXAnnotation(**mock_gdax_open_order)
    assert annotation.open_order() == {
        'measurement': 'open',
        'time': '2017-12-25T00:45:37.141000Z',
        'fields': {
            'title': 'open order',
            'text': json.dumps(mock_gdax_open_order),
            'tags': '300.00000000,LTC-EUR,sell,open'
        }
    }


def test_done_order(mock_gdax_cancelled_order):
    """It returns an annotation for a cancelled order."""
    annotation = GDAXAnnotation(**mock_gdax_cancelled_order)
    assert annotation.done_order() == {
        'measurement': 'done',
        'time': '2017-12-25T00:30:37.727000Z',
        'fields': {
            'title': 'done order',
            'text': json.dumps(mock_gdax_cancelled_order),
            'tags': '300.00000000,LTC-EUR,canceled,sell,done'
        }
    }


def test_received_order(mock_gdax_received_order):
    """It returns an annotation for a received order."""
    annotation = GDAXAnnotation(**mock_gdax_received_order)
    assert annotation.received_order() == {
        'measurement': 'received',
        'time': '2017-12-25T00:45:37.141000Z',
        'fields': {
            'title': 'received order',
            'text': json.dumps(mock_gdax_received_order),
            'tags': '617b5d68-d262-413a-852a-4420c1208e4b,limit,300.00000000,LTC-EUR,sell,received'
        }
    }


def test_has_database():
    """It returns True if the database is present."""
    assert has_database('test_db', ('test_db', 'other_db'))


def test_list_databases():
    """It returns the correct list of databases."""
    client = influxdb.InfluxDBClient()
    client.get_list_database = mock.MagicMock(return_value=[
        {'name': 'test_db'},
        {'name': 'foo_db'}
    ])
    assert list(list_databases(client)) == ['test_db', 'foo_db']
