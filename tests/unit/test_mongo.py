"""Unit tests related to the `mongo` module."""
from datetime import datetime, timedelta

from pymongo import MongoClient

from mongo import get_client, make_event_query
from freezegun import freeze_time


def test_get_client():
    """It returns an instance of the correct client class."""
    assert isinstance(get_client('testhost', 27017), MongoClient)


@freeze_time('2017-07-27')
def test_make_event_query_default():
    """It returns the correct default query."""
    assert make_event_query('test_event') == {
        'time': {
            '$gte': '2017-07-26T23:55:00.000000Z'
        },
        'type': 'test_event'
    }


@freeze_time('2017-07-27')
def test_make_event_query_options():
    """It returns the correct query when options are present."""
    options = {
        'start': timedelta(**{'minutes': 1}),
        'end': datetime.utcnow()
    }
    assert make_event_query('test_event', **options) == {
        'time': {
            '$gte': '2017-07-26T23:59:00.000000Z'
        },
        'type': 'test_event'
    }
