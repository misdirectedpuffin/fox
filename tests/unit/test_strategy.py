"""Unit tests for compare module."""
from datetime import datetime
import pytest

from strategy import MarketMaker, Straddle, has_open_position, OrderBook


def test_spread(mock_order_book):
    """Test that it returns the expected spread"""
    book = OrderBook(mock_order_book)
    assert book.spread == 0.00018000


def test_get_bid_ask(mock_order_book):
    """Test that it returns the correct value."""
    # TODO: Test that this raises an exception.
    book = OrderBook(mock_order_book)
    assert book.get_bid_ask('bids') == 0.001200


def test_buydown(mock_order_book):
    """It returns the correct buydown value."""
    straddle = Straddle(
        mock_order_book,
        293.00,
        pct_profit=0.02,
    )
    assert straddle.buydown(0.00018000, interval=0.05) == 292.999991


def test_sell_price(mock_order_book):
    """Test that the sell price returns correctly."""
    straddle = Straddle(
        mock_order_book,
        43.00,
        pct_profit=0.02,
    )
    assert straddle.sell_price() == 43.86


def test_has_open_position():
    """Test that there are currently two open orders."""
    orders = [{'type': 'buy'}, {'type': 'sell'}]
    assert has_open_position(orders)


def test_parse_order():
    """Test that the order is parsed to the correct types."""
    order = {
        'amount': '0.12000000',
        'date': '2017-08-02 14:19:28',
        'margin': 0,
        'orderNumber': '96012642299',
        'rate': '0.00157148',
        'startingAmount': '0.12000000',
        'total': '0.00018857',
        'type': 'buy'
    }
    maker = MarketMaker('bar', 'baz')
    assert maker.parse_order(order) == {
        'amount': 0.12000000,
        'date':  datetime(2017, 8, 2, 14, 19, 28),
        'margin': 0,
        'orderNumber': 96012642299,
        'rate': 0.00157148,
        'startingAmount': 0.12000000,
        'total': 0.00018857,
        'type': 'buy'
    }


def test_mark_as_complete(order_with_resulting_trade):
    """It creates a key on the order with a `True` value."""
    maker = MarketMaker('bar', 'BTC_LTC')
    order = order_with_resulting_trade[0]
    maker.mark_as_complete(order)
    assert order.get('complete')


def test_mark_as_straddle(order_with_resulting_trade):
    """It marks orders as being part of the straddle."""
    maker = MarketMaker('bar', 'BTC_LTC')
    orders = maker.mark_as_straddle(order_with_resulting_trade)
    for order in orders:
        assert order.get('is_straddle')


def test_is_valid(order_with_resulting_trade):
    """It returns orders that completed immediately."""
    buy_order = {
        "orderNumber": 31226040,
        "resultingTrades": []
    }
    sell_order = order_with_resulting_trade[0]
    maker = MarketMaker('session', 'BTC_LTC')
    assert list(maker.completed_immediately(buy_order, sell_order)) == [
        sell_order
    ]
