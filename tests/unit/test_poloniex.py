"""Tests related to poloniex."""

from datetime import datetime

import pytest
from freezegun import freeze_time

from poloniex import (
    is_successful,
    is_valid_amount,
    Poloniex,
    trade_history_payload
)


def test_make_headers():
    """Test that the headers for Poloninex api calls are created correctly."""
    poloniex = Poloniex('foo', 'secret')
    assert poloniex.make_headers(
        **{'thing': 'other', 'you': 'bae'}
    ) == {'Key': 'foo',
          'Sign': (
              '956192cfadd68b9ca95af31e0c596aae'
              '80fdf8c86560ff51c85f7a01b2fe280f'
              'd86e0c4f453395645d829b95fa86a718'
              'f22a1b2d32d97573a4e2cebd95fe4944'
          ),
          'content-type': 'application/x-www-form-urlencoded'}


@pytest.mark.parametrize(
    'currency_pair, start, end, expected', [
        (
            'BTC_XCP',
            datetime(2017, 1, 1),
            datetime(2017, 1, 2),
            {
                'start': 1483228800000,
                'end': 1483315200000,
                'currencyPair': 'BTC_XCP',
                'command': 'returnTradeHistory'
            }
        ),
        (
            'BTC_XCP',
            None,
            datetime(2017, 1, 2),
            {
                'start': 1501027200000,
                'end': 1483315200000,
                'currencyPair': 'BTC_XCP',
                'command': 'returnTradeHistory'
            }
        ),
        (
            'BTC_XCP',
            datetime(2017, 1, 1),
            None,
            {
                'start': 1483228800000,
                'end': 1501113600000,
                'currencyPair': 'BTC_XCP',
                'command': 'returnTradeHistory'
            }
        ),
        (
            'BTC_XCP',
            None,
            None,
            {
                'start': 1501027200000,
                'end': 1501113600000,
                'currencyPair': 'BTC_XCP',
                'command': 'returnTradeHistory'
            }
        )
    ]
)
@freeze_time('2017-07-27')
def test_trade_history_payload(currency_pair, start, end, expected):
    """
    Test that the payload to be sent in `trade_history` is created
    correctly.
    """
    assert trade_history_payload(currency_pair, start, end) == expected


@pytest.mark.parametrize(
    'amount, expected', [
        (48.00, True),
        (0.00300, True)
    ]
)
def test_is_valid_amount(amount, expected):
    """Test the the order's validity is checked."""
    assert is_valid_amount(amount) == expected


def test_is_valid_amount_false():
    """It returns False when the amount is invalid."""
    assert pytest.xfail()


@pytest.mark.parametrize(
    'cancellation, expected', [
        ({'success': 1}, True),
        ({'success': 0}, False)
    ]
)
def test_is_successful(cancellation, expected):
    """Test that the cancellation of an order is checked."""
    assert is_successful(cancellation) == expected

# def test_cancel_orders():
#     """Test that the correct function is created."""
#     poloniex = Poloniex('foo', 'bar')
#     poloniex.cancel_orders('LTC_BTC', ['123', '456'])


def test_invalid_cancellation():
    """It returns invalid cancellations."""
    cancellations = [
        {'success': 1},
        {'success': 0}
    ]
    poloniex = Poloniex('key', 'secret')
    assert list(poloniex.invalid_cancellations(cancellations)) == [
        {'success': 0}
    ]


def test_valid_trade_true(order_with_resulting_trade):
    """It tests that the trade returned without errors."""
    mock_trade = order_with_resulting_trade[0]
    poloniex = Poloniex('key', 'secret')
    assert poloniex.valid_trade(mock_trade)


def test_valid_trade_false():
    """It test that the trade returned with errors."""
    poloniex = Poloniex('key', 'secret')
    mock_trade = {'error': 'the message'}
    assert not poloniex.valid_trade(mock_trade)
