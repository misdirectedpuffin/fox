"""Tests related to the utilities module."""

from datetime import datetime

import pytest

from util import (datetime_to_timestamp, get_logfile_name, get_stop_limit,
                  parse_secret)


@pytest.mark.parametrize(
    'secret, name, expected', [
        ('{"password": "1234"}', 'password', '1234'),
        ('{"password": "1234"}', None, {'password': '1234'}),
    ]
)
def test_parse_secret(secret, name, expected):
    """Assert parse_secret returns the correct item."""
    assert parse_secret(secret, name=name) == expected


@pytest.mark.parametrize(
    'date_time, expected', [
        (datetime(2017, 1, 1), 1483228800000)
    ]
)
def test_datetime_to_timestamp(date_time, expected):
    """Test that the datetime is converted to unix timestamp."""
    assert datetime_to_timestamp(date_time) == expected


def test_get_logfile_name():
    """Test that the expected logfile path is returned."""
    assert get_logfile_name('/path/to/file') == '/path/to/logs/file.log'


def test_get_stop_limit():
    """It returns the correct stop limit."""
    assert get_stop_limit(100, 90) == -10
