"""Integration tests relating to MongoDB."""
from freezegun import freeze_time

from conftest import MockMongoCollection
from mongo import get_documents, get_collection


@freeze_time('2018-01-01 15:32:00')
def test_get_documents():
    """It retrieves the correct document from mongodb."""
    event = 'open'
    with MockMongoCollection(event) as collection:
        mock_query = {
            'time': {
                '$gte': '2018-01-01T15:32:00.000000Z'
            },
            'type': event
        }
        assert list(get_documents(mock_query, collection)) == [
            {
                "type": "open",
                "side": "sell",
                "price": "300.00000000",
                "order_id": "1bbc73dc-7dda-4db5-ab42-bb683aab101a",
                "remaining_size": "1.00000000",
                "product_id": "LTC-EUR",
                "sequence": 403655937,
                "user_id": "59ac1146e2c4ee02d62e13f5",
                "profile_id": "c077a092-38b2-4a98-8359-08c70d31fbeb",
                "time": "2018-01-01T15:32:00.000000Z",
            }
        ]


def test_get_collection():
    """It returns the correct mongodb collection."""
    event = 'open'
    with MockMongoCollection(event) as database:
        assert get_collection(event, database) == database[event]
