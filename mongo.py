"""Wrappers to help query mongodb."""
from datetime import datetime, timedelta

from pymongo import MongoClient

from constants import INFLUX_TIMESTAMP_FORMAT


def get_client(hostname, port):
    """Return the client connection object."""
    return MongoClient(hostname, port)


def get_documents(query, mongo_collection):
    """Get a selection of events from MongoDB

    Args:
        query (dict): Represents the query to be passed to .find().
        mongo_collection (dict): The mongo collection.
    Yields: Mongodb documents.
    """
    for doc in mongo_collection.find(query):
        doc.pop('_id')  # Not json serializable
        yield doc


def make_event_query(event, **options):
    """Return events of `type` for a period."""
    end = options.get('end', datetime.utcnow())
    start = options.get('start', timedelta(**{'minutes': 5}))
    last = end - start
    fmt = options.get('format', INFLUX_TIMESTAMP_FORMAT)
    return {
        'time': {
            '$gte': last.strftime(fmt)
        },
        'type': event
    }


def get_collection(event_type, mongodb):
    """Return the mongodb collection for a websocket api event."""
    return mongodb[event_type]
