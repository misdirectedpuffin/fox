"""Package level test fixtures."""
import json
from datetime import datetime

import pytest
from pymongo import MongoClient


@pytest.fixture
def mock_order_book():
    """Return a mock order book."""
    return {
        'bids': [
            ['0.001200', 20.00],
            ['0.001300', 30.00],
            ['0.001300', 40.00]
        ],
        'asks': [
            ['0.001299', 0.1],
            ['0.001300', 0.2],
            ['0.001380', 40]
        ],
        'isFrozen': '0',
        'seq': 122860295
    }


@pytest.fixture
def transformed_response():
    """Return a mock of the transformed Poloniex api response data."""
    return [
        {
            "id": 123,
            "last": "44.45000003",
            "lowestAsk": "44.63999795",
            "highestBid": "44.45000003",
            "percentChange": "-0.00214379",
            "baseVolume": "8363760.30505012",
            "quoteVolume": "182616.92200237",
            "isFrozen": "0",
            "high24hr": "47.98899999",
            "low24hr": "43.66310794",
            "pair": "USDT_LTC"
        },
        {
            "id": 124,
            "last": "0.12700000",
            "lowestAsk": "0.12828800",
            "highestBid": "0.12680000",
            "percentChange": "0.05394190",
            "baseVolume": "2762153.86975084",
            "quoteVolume": "21742882.54664946",
            "isFrozen": "0",
            "high24hr": "0.13800000",
            "low24hr": "0.11350000",
            "pair": "USDT_NXT"
        },
        {
            "id": 8,
            "last": "0.00006415",
            "lowestAsk": "0.00006420",
            "highestBid": "0.00006406",
            "percentChange": "-0.02225270",
            "baseVolume": "53.19286882",
            "quoteVolume": "833397.16564967",
            "isFrozen": "0",
            "high24hr": "0.00006736",
            "low24hr": "0.00006000",
            "pair": "TC_BELA"
        },
        {
            "id": 10,
            "last": "0.00011434",
            "lowestAsk": "0.00011434",
            "highestBid": "0.00011333",
            "percentChange": "0.10848279",
            "baseVolume": "84.97337589",
            "quoteVolume": "753201.67088777",
            "isFrozen": "0",
            "high24hr": "0.00012007",
            "low24hr": "0.00010099",
            "pair": "BTC_BLK"
        }
    ]


@pytest.fixture
def mock_poloniex_response():
    """Return a mock of the poloninex ticker api."""
    return {
        "USDT_LTC": {
            "id": 123,
            "last": "44.45000003",
            "lowestAsk": "44.63999795",
            "highestBid": "44.45000003",
            "percentChange": "-0.00214379",
            "baseVolume": "8363760.30505012",
            "quoteVolume": "182616.92200237",
            "isFrozen": "0",
            "high24hr": "47.98899999",
            "low24hr": "43.66310794"
        },
        "USDT_NXT": {
            "id": 124,
            "last": "0.12700000",
            "lowestAsk": "0.12828800",
            "highestBid": "0.12680000",
            "percentChange": "0.05394190",
            "baseVolume": "2762153.86975084",
            "quoteVolume": "21742882.54664946",
            "isFrozen": "0",
            "high24hr": "0.13800000",
            "low24hr": "0.11350000"
        },
        "TC_BELA": {
            "id": 8,
            "last": "0.00006415",
            "lowestAsk": "0.00006420",
            "highestBid": "0.00006406",
            "percentChange": "-0.02225270",
            "baseVolume": "53.19286882",
            "quoteVolume": "833397.16564967",
            "isFrozen": "0",
            "high24hr": "0.00006736",
            "low24hr": "0.00006000"
        },
        "BTC_BLK": {
            "id": 10,
            "last": "0.00011434",
            "lowestAsk": "0.00011434",
            "highestBid": "0.00011333",
            "percentChange": "0.10848279",
            "baseVolume": "84.97337589",
            "quoteVolume": "753201.67088777",
            "isFrozen": "0",
            "high24hr": "0.00012007",
            "low24hr": "0.00010099"
        },
    }


@pytest.fixture
def mock_tickers():
    """Return a list of tickers."""
    return [
        {
            'price': 25.5753,
            'currency': 'USD',
            'timestamp': datetime(2017, 6, 1, 2, 4, 1),
            'id': 1481,
            'ewma': 25.477610613909388,
            'difference': 0.097689386090610952
        },
        {
            'price': 28.591,
            'currency': 'USD',
            'timestamp': datetime(2017, 6, 2, 2, 4, 1),
            'id': 1482,
            'ewma': 26.515407075939592,
            'difference': 2.075592924060409
        }
    ]


@pytest.fixture
def mock_influxdb_response():
    """Mock a ResultSet object from influx, which is a type of dict.
    There are other fields such as 'lowestAsk', etc but we only need a
    single field for the purposes of the test because we rely on a float
     and a timestamp.
    """
    return {
        "USDT_LTC": [
            {
                "last": 42.11000001,
                "time": "2017-07-26T00:00:47Z"
            },
            {
                "last": 42.11000001,
                "time": "2017-07-26T00:01:47Z"
            },
            {
                "last": 42.27609996,
                "time": "2017-07-26T00:02:47Z"
            },
            {
                "last": 42.30000001,
                "time": "2017-07-26T00:03:47Z"
            },
            {
                "last": 42.53894609,
                "time": "2017-07-26T00:04:47Z"
            },
            {
                "last": 42.566,
                "time": "2017-07-26T00:05:47Z"
            }
        ]
    }


@pytest.fixture
def mock_poloniex_orders():
    """Return a mocked list of orders from the Poloniex exchange."""
    return [
        {
            'amount': '0.12000000',
            'date': '2017-08-02 15:39:30',
            'margin': 0,
            'orderNumber': '96012642299',
            'rate': '0.00137148',
            'startingAmount': '0.12000000',
            'total': '0.00018857',
            'type': 'buy'
        },
        {
            'amount': '0.13050600',
            'date': '2017-08-02 15:39:28',
            'margin': 0,
            'orderNumber': '96012642223',
            'rate': '0.00157148',
            'startingAmount': '0.11000000',
            'total': '0.00018857',
            'type': 'sell'
        },
        {
            'amount': '0.12000000',
            'date': '2017-08-01 14:19:28',
            'margin': 0,
            'orderNumber': '96015662296',
            'rate': '0.00157148',
            'startingAmount': '0.12900000',
            'total': '0.00018857',
            'type': 'buy'
        },
        {
            'amount': '0.12000000',
            'date': '2017-07-27 14:19:28',
            'margin': 0,
            'orderNumber': '86012666699',
            'rate': '0.00157148',
            'startingAmount': '0.13000000',
            'total': '0.00018857',
            'type': 'buy'
        }
    ]


@pytest.fixture
def order_with_resulting_trade():
    """Return a Poloniex order with a resulting trade."""
    return [
        {
            "orderNumber": 31226040,
            "resultingTrades": [
                {
                    "amount": "338.8732",
                    "date": "2014-10-18 23:03:21",
                    "rate": "0.00000173",
                    "total": "0.00058625",
                    "tradeID": "16164",
                    "type": "buy"
                }
            ]
        }
    ]


@pytest.fixture
def mock_gdax_ticker():
    """Return ticker json as a python object."""
    with open('./tests/fixtures/GDAX/ticker.json') as file:
        data = file.read()
        return json.loads(data)


@pytest.fixture
def mock_gdax_open_order():
    """Return open order json as a python object."""
    with open('./tests/fixtures/GDAX/open_order.json') as file:
        data = file.read()
        return json.loads(data)


@pytest.fixture
def mock_gdax_cancelled_order():
    """Return cancelled order json as a python object."""
    with open('./tests/fixtures/GDAX/cancelled_order.json') as file:
        data = file.read()
        return json.loads(data)


@pytest.fixture
def mock_gdax_received_order():
    """Return received order json as a python object."""
    with open('./tests/fixtures/GDAX/received_order.json') as file:
        data = file.read()
        return json.loads(data)


@pytest.fixture
def mock_mongo_ticker_document():
    """Return mongodb ticker json as a python object."""
    path = './tests/fixtures/mongodb/mongo_document_ticker.json'
    with open(path) as file:
        data = file.read()
        return json.loads(data)


class MockMongoCollection(object):
    """Connection to a mongodb instance providing mock test data."""

    def __init__(self, event, path=None):
        self.event = event
        self._connection = None
        self._path = path

    @property
    def connection(self):
        """MongoDB connection."""
        if self._connection is None:
            self._connection = MongoClient('mongo', 27017).testdb
        return self._connection

    @property
    def path(self):
        """Return the path to collection fixture."""
        if self._path is None:
            path = './tests/integration/{}_collection.json'
            self._path = path.format(self.event)
        return self._path

    def __enter__(self):
        collection = self.connection[self.event]
        with open(self.path) as file:
            documents = json.loads(file.read())
            collection.insert_many(documents)
        return collection

    def __exit__(self, err_type, value, traceback):
        self.connection.drop_collection(self.event)
