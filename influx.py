"""Helper functions related to influxdb."""
import json
from datetime import datetime
from textwrap import dedent

import pandas as pd

from constants import INFLUX_TIMESTAMP_FORMAT, DEFAULT_GDAX_ANNOTATION_TAGS


def make_influx_request(client, querystring):
    """Return the data from the query.

    client (InfluxClient): The client object.
    querystrint (str): The query to send to influxdb.
    """
    return client.query(querystring)


class InfluxPoloniex(object):
    """Interface to influxdb for Poloniex data."""

    def __init__(self):
        pass

    @staticmethod
    def make_poloniex_influx_datapoint(**item):
        """Return a single influxdb metric point."""
        if item.get('measurement') == 'ewma':
            field = item['field']
            return {
                'tags': {
                    'ticker': item['pair'],
                    'ewma': item['field']
                },
                'measurement': 'ewma',
                'fields': {
                    item['field']: item[field]
                },
                'time': item['time']
            }

        new_item = {
            'tags': {
                'ticker': item['pair']
            },
            'time': datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ'),
            'measurement': item['pair'],
        }
        # Remove the ticker pair name from fields. It's present in tags
        # so we don't need it here, too.
        item.pop('pair')
        new_item['fields'] = item
        return new_item

    @staticmethod
    def make_ewma_records(influx_response, pair, field, span=3):
        """Return the exponential weighted moving average datapoints.

        Args:
            influx_response (influx.resultset.ResultSet): The response from
                influxdb.
            pair (str): The ticker pair to use. E.g. USDT_BTC.
            field (str): The key of the ticker items to base the ewma on.
        Kwargs:
            san (int): The span of datapoints to use.

        Yields:
            dict: Next item in series of dicts in the format.
            {
                'last': 42.11000001,
                'field': 'last',
                'pair': 'USDT_LTC',
                'time': '2017-07-26T00:00:47Z'
            }
        """
        field_name = pd.Series(
            [field for item in influx_response[pair]],
            name='field'
        )
        frame = pd.DataFrame(field_name)
        frame['measurement'] = pd.Series(
            ['ewma' for item in influx_response[pair]],
            name='measurement'
        )
        frame['values'] = pd.Series(
            [p[field] for p in influx_response[pair]], name='values'
        )
        frame[field] = pd.ewma(frame['values'], span=float(span))
        frame['time'] = pd.Series(
            [p['time'] for p in influx_response[pair]],
            name='time'
        )
        frame['pair'] = pd.Series(
            [pair for p in influx_response[pair]],
            name='pair'
        )

        # Remove the field and values after used to calc ewma.
        del frame['values']
        for record in frame.to_dict(orient='records'):
            yield record

    @staticmethod
    def annotate(**fields):
        """Return an annotation that can be sent to influxdb.

        Args:
            title (str): The title of the annotation.
            text (str): The text to send in the annotation.
            tags (str): Comma separated string of tags.
            date_time (datetime): datetime object.
        Returns
            Annotation in the form of a dictionary.
        """
        date_time = fields.get('date_time')
        if date_time is None:
            date_time = datetime.utcnow()
        data = {
            'measurement': 'events',
            'time': date_time.strftime('%Y-%m-%d %H:%M:%S'),
            'fields': {'title': fields.get('title')}
        }
        text = fields.get('text')
        tags = fields.get('tags')
        if text is not None:
            data['fields'].update(**{'text': text})
        if tags is not None:
            data['fields'].update(**{'tags': tags})
        return data

    @staticmethod
    def make_querystring(fmt='%Y-%m-%d %H:%M:%S', **options):
        """Make the influx querystring according to the ticker.

        Args:
            field (str): The field to select the value of.
            measurement (str): The measurement.
            ticker (str): The ticker string E.g. USDT_LTC
            start (datetime): The start datetime.
            end (datetime): The end datetime.
        Kwargs:
            fmt (str): date string format.
        Returns:
            query (str): query to send to influxdb via the client.
        """
        query = dedent('''
        SELECT "{field}" FROM "{measurement}" WHERE \
        "ticker" = '{ticker}' AND \
        time > '{start}' AND \
        time <= '{end}' \
        GROUP BY "ticker"
        ''')
        start = options.get('start').strftime(fmt)
        end = options.get('end').strftime(fmt)
        return query.format(
            field=options.get('field'),
            measurement=options.get('measurement'),
            ticker=options.get('ticker'),
            start=start,
            end=end
        )

    def create_annotations(self, orders, title=None, tags=None):
        """Return a list of payloads to create annotations."""
        return [
            self.annotate(**{
                'title': str(title),
                'text': json.dumps(order),
                'tags': str(tags),
                'date_time': datetime.utcnow()
            }) for order in orders
        ]


def gdax_datapoint_factory(**item):
    """Return an influx ticker datapoint with GDAX data.

    Args:
        item (dict): An `order` or `ticker` document from a
    mongodb collection. Order documents create annotations,
    tickers create datapoints.
    """
    annotation = GDAXAnnotation(**item)
    return {
        'ticker': annotation.ticker,
        'open': annotation.open_order,
        'done': annotation.done_order,
        'received': annotation.received_order
    }.get(item.get('type'))


def has_database(database_name, databases):
    """Return True if the database exists."""
    return database_name in databases


def list_databases(client):
    """Return a list of available databases from influxdb."""
    databases = map(lambda db: db['name'], client.get_list_database())
    for database_name in databases:
        yield database_name


class GDAXAnnotation(object):
    """Interface to handle sending GDAX data to influxdb."""

    def __init__(self, default_tags=DEFAULT_GDAX_ANNOTATION_TAGS, **fields):
        self.fields = fields
        self.default_tags = default_tags

    def _make_tag_format_string(self, tags=None):
        """Return a string of comma separated tags.
        Args:
            tags (list): A list of strings.
        Returns:
            tags: A comma separated string of tags.
        """
        if tags is None:
            updated_default_tags = self.default_tags
        else:
            updated_default_tags = self.default_tags + tags
            updated_default_tags = sorted(set(updated_default_tags))
        return ','.join(['{{{}}}'.format(tag) for tag in updated_default_tags])

    def check_timestamp(self):
        """Check that a timestamp exists or create one."""
        timestamp = self.fields.get('time')
        if timestamp is None:
            timestamp = datetime.utcnow()
            timestamp = timestamp.strftime(INFLUX_TIMESTAMP_FORMAT)
        return timestamp

    def ticker(self):
        """Representation of the ticker from a mongdodb document."""
        item = self.fields
        tags = {
            'product_id': item['product_id'],
            'type': item.get('type'),
            'side': item.get('side')
        }
        timestamp = item['time']

        item.pop('product_id')
        item.pop('type')
        item.pop('time')
        item.pop('side')

        return {
            'tags': tags,
            'fields': item,
            'time': timestamp,
            'measurement': 'ticker'
        }

    def _make_annotation(self, tags):
        """Make an annotation based on tags and self.fields"""
        fields = self.fields
        order_type = fields.get('type')
        return {
            'measurement': order_type,
            'time': self.check_timestamp(),
            'fields': {
                'text': json.dumps(fields),
                'title': '{} order'.format(order_type),
                'tags': tags
            }
        }

    def open_order(self):
        """Return annotation data point for an open order."""
        tags = self._make_tag_format_string()
        return self._make_annotation(tags.format(**self.fields))

    def done_order(self):
        """Return annotation data point for a done order."""
        tags = self._make_tag_format_string(tags=['reason'])
        return self._make_annotation(tags.format(**self.fields))

    def received_order(self):
        """Return annotation data point for a received order."""
        tags = self._make_tag_format_string(tags=['order_type', 'order_id'])
        return self._make_annotation(tags.format(**self.fields))
