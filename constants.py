"""Constants used throughout the application."""
from datetime import datetime, timedelta

INFLUX_TIMESTAMP_FORMAT = '%Y-%m-%dT%H:%M:%S.%fZ'

DEFAULT_GDAX_ANNOTATION_TAGS = [
    'price',
    'product_id',
    'side',
    'type'
]

# When gathering websocket data streamed to mongodb, gather from these
# collections.
MONGODB_COLLECTIONS = (
    'ticker',
    'open',
    'done',
    'received'
)

# Mapping of channels and products to subscribe to.
GDAX_SUBSCRIPTIONS = [
    ('heartbeat', ['LTC-EUR']),
    ('ticker', ['LTC-EUR']),
    ('user', ['LTC-EUR'])
]

# The period for which we query mongodb.
MONGODB_QUERY_OPTIONS = {
    'format': INFLUX_TIMESTAMP_FORMAT,
    'start': timedelta(**{'minutes': 5}),
    'end': datetime.utcnow()
}
