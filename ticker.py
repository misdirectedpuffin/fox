"""Module to collect ticker data from mongodb and write to influxdb."""
import logging
import sched
import time

import requests
from influxdb import InfluxDBClient

from constants import MONGODB_COLLECTIONS, MONGODB_QUERY_OPTIONS
from influx import gdax_datapoint_factory, has_database, list_databases
from mongo import get_client, get_collection, get_documents, make_event_query
from util import get_credentials, parse_secret, setup_logging


LOGGING = setup_logging(logging, 'ticker')
LOGGER = LOGGING.getLogger(__name__)


def connect_mongo():
    """Connect to mongodb and return a db `gdax`."""
    try:
        # Connect to mongodb.
        mongo = get_client('mongo', 27017)
        return mongo.gdax
    except:
        LOGGER.error('Failed to connect MongoDB.')


def retrieve_authentication(credential, name=None):
    """Retrieve and parse the credentials from credstash.

    Args:
        credential (dict): The loaded json from credstash.
        name (str): The name of the node to parse rather than the whole
            credential document.
    Returns:
        auth_data (dict): A dictionary of password/secret data.
    """
    try:
        resp = get_credentials(credential)
    except:
        LOGGER.error('Failed to retrieve credentials from credstash')
    else:
        return parse_secret(resp, name=name)


def gather_documents(mongodb, *event_types, **options):
    """Yield documents for each event type."""
    for event in event_types:
        try:
            query = make_event_query(event, **options)
            try:
                collection = get_collection(event, mongodb)
            except Exception as exc:
                LOGGER.error(
                    'Failed to retrieve collection: %s, %s',
                    event,
                    str(exc)
                )
            else:
                LOGGER.debug('Successfully retrieved collection: %s: ', event)
        except Exception as exc:
            LOGGER.error(
                'Failed to retrieve %s documents: %s',
                event, str(exc)
            )

        for doc in get_documents(query, collection):
            yield doc


def write_influx_datapoints(client, points):
    """Write the data to influxdb."""
    try:
        client.write_points(points)
        LOGGER.debug('Datapoints sent to influxdb.')

    except requests.exceptions.ConnectionError as exc:
        LOGGER.warning('Failed to connect to influxdb: %s', str(exc))

    except requests.exceptions.HTTPError as exc:
        LOGGER.warning('HttpError influxdb: %s', str(exc))

    except requests.exceptions.Timeout as exc:
        LOGGER.warning('Request to influxdb timed out: %s', str(exc))
    except Exception as exc:
        LOGGER.warning('Something went wrong: %s', str(exc))


def report(schedule, client, influx_db, **options):
    """Write ticker points to influxdb every minute.

    Args:
        client (influxdb.InfluxDBClient): The client.
        influx_db (str): The name of the database to use.
    """
    databases = list_databases(client)

    # Check that the db exists or create it.
    if not has_database(influx_db, databases):
        client.create_database(influx_db)

    documents = list(
        gather_documents(
            connect_mongo(),
            *MONGODB_COLLECTIONS,
            **options
        )
    )

    if any(documents):
        LOGGER.info('MongoDB: Documents found.')
        points = [gdax_datapoint_factory(**point)() for point in documents]
    else:
        LOGGER.warning('No documents retrieved from mongodb.')
        points = []

    if any(points):
        write_influx_datapoints(client, points)
    else:
        LOGGER.warning('No influx data points to write.')

    schedule.enter(
        60,
        1,
        report,
        (schedule, client, influx_db),
        kwargs=options
    )
    LOGGER.info('rescheduled...')


def main():
    """Entrypoint."""
    authentication = retrieve_authentication('GDAX', name='INFLUX')
    influx = InfluxDBClient(**authentication)
    db_name = authentication['database']

    schedule = sched.scheduler(time.time, time.sleep)
    schedule.enter(
        60,
        1,
        report,
        (schedule, influx, db_name),
        kwargs=MONGODB_QUERY_OPTIONS
    )
    schedule.run()

if __name__ == '__main__':
    main()
