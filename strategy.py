"""Test interpretation of ticker data."""

import logging
from datetime import datetime
from operator import eq

from poloniex import InvalidOrder

logging.basicConfig(
    # filename=get_logfile_name(os.path.abspath(__name__)),
    level=logging.INFO,
    format='%(asctime)s::%(levelname)s::%(name)s::%(message)s'

)
LOGGER = logging.getLogger(__name__)


class MarketMaker(object):
    """Market maker trading strategy."""

    def __init__(self, session, currency_pair):
        """Initialise the strategy with an exchange and a ticker pair.

        session (sqlalchemy.orm.session.sessionmaker): db connection
        currency_pair (str): The currency pair to trade.
        """
        self.session = session
        self.currency_pair = currency_pair

    def transact(self, orders):
        """Create an order and log it's details to the db.

        Args:
            orders (dict): The orders returned from the api response.
        Yields:
            boolean: If the transaction succeeded.
        """
        for order in orders:
            order = self.parse_order(order)
            try:
                try:
                    self.session.add()
                    self.session.commit()
                except:
                    self.session.rollback()

            except InvalidOrder as exc:
                LOGGER.info(exc.message)
            else:
                LOGGER.info('DB transaction successful!: %s', str(order))
                yield True

    def parse_order(self, response):
        """
        Process the api response, transforming the string values
        into types suitable for sqlalchemy.

        Args:
            response (dict): The order response as a dict.
        Returns:
            dict: The new, parsed dictionary.
        """
        new = {}
        for key, val in response.items():
            if key == 'resultingTrades':
                for trade in val:
                    trade['complete'] = True
                    self.parse_order(trade)
            elif key == 'date':
                new[key] = datetime.strptime(val, '%Y-%m-%d %H:%M:%S')
            elif key == 'orderNumber':
                new[key] = int(val)
            elif key == 'type':
                new[key] = str(val)
            else:
                new[key] = float(val)
        return new

    def is_open(self, order):
        """Return True if the order is open."""
        pass

    @staticmethod
    def mark_as_complete(order):
        """Mark the order as complete."""
        order['complete'] = True
        return order

    @staticmethod
    def mark_as_straddle(orders):
        """Return a new dict with the `is_straddle` attribute.

        Args:
            orders (list): a list of order dicts returned from a
            successful buy.
        Yields:
            order (dict): The updated order to be passed to the db.
        """
        for order in orders:
            new_order = {'is_straddle': True}
            yield dict(order, **new_order)

    def completed_immediately(self, buy_order, sell_order):
        """Return True if the orders completed immediately."""
        # Orders should be close enough to the initial buy that they
        # instantly complete. If they don't we need to log them to the db
        # to be checked later.
        return filter(self.has_resulting_trades, (sell_order, buy_order))

    @staticmethod
    def has_resulting_trades(order):
        """Return True if the order is immediately fulfilled."""
        return any(order.get('resultingTrades'))


def has_open_position(orders):
    """Return True if there is a buy and sell order open."""
    if any(orders):
        return eq(max(num for num, item in enumerate(orders, 1)), 2)
    return False


class OrderBook(object):
    """Class representing the order book."""

    def __init__(self, order_book):
        self.order_book = order_book
        self.bids = order_book['bids']
        self.asks = order_book['asks']

    @property
    def bid(self):
        """Return the lowest bid (buy) on the order book."""
        return round(float(min(self.bids, key=lambda bid: bid[0])[0]), 8)

    @property
    def ask(self):
        """Return the highest ask (sell) on the order book."""
        return round(float(max(self.asks, key=lambda ask: ask[0])[0]), 8)

    def get_bid_ask(self, side, func=min):
        """Get the highest or lowest value of the bid or ask.

        Args:
            side (str): The side of the order book. Bid or ask.
            func (object): Min or max function
        Return:
            Float: The max or min bid or ask. To precision of 8 decimal
                places.
        """
        if not side.endswith('s'):
            raise ValueError('The \'side\' argument must be pluralised.')
        value = (float(order[0]) for order in self.order_book[side])
        return round(func(value), 8)

    @property
    def spread(self):
        """Return the bid ask spread."""
        return round(abs(self.ask - self.bid), 8)


class Straddle(object):
    """Class representing the straddle."""

    def __init__(self, order_book, price, pct_profit=0.05):
        self.order_book = order_book
        self.price = float(price)  # entry price
        self.pct_profit = pct_profit

    def buydown(self, spread, interval=0.05):
        """Buy price."""
        buydown = spread * interval
        LOGGER.debug('price: %s, buydown interval: %s', self.price, buydown)
        return round(self.price - buydown, 8)

    def sell_price(self):
        """Return the price to sell at to make a profit."""
        return self.price * (1 + self.pct_profit)

    @staticmethod
    def _filter_order_type(orders, order_type='buy'):
        """Filter orders based on order_type."""
        # TODO: itemgetter

        def is_order_type(order):
            """Return True if `order` is of `order_type`"""
            return order.order_type == order_type

        for order in filter(is_order_type, orders):
            yield order

    def spread(self, orders):
        """The value of the straddle.

        The difference in curency between the lowest sell and
        the highest buy.

        Args:
            open_orders (list): the open orders in the db.
        Returns:
            float: the straddle difference

        """
        orders = filter(lambda o: o.complete, orders)
        # orders = (dict(r) for r in open_orders)
        lowest_sell = min(
            self._filter_order_type(orders, order_type='sell'),
            key=lambda o: o.rate
        )
        highest_buy = max(
            self._filter_order_type(orders, order_type='buy'),
            key=lambda o: o.rate
        )

        return abs(highest_buy - lowest_sell)


class StraddleException(Exception):
    """Failed to create a straddle position."""
